/****************************************************************
 *								*
 * Copyright (c) 2001-2024 Fidelity National Information	*
 * Services, Inc. and/or its subsidiaries. All rights reserved.	*
 *								*
 *	This source code contains the intellectual property	*
 *	of its copyright holder(s), and is made available	*
 *	under a license.  If you do not know the terms of	*
 *	the license, please stop and do not read further.	*
 *								*
 ****************************************************************/

#ifndef GETSTORAGE_INCLUDED
#define GETSTORAGE_INCLUDED

int4 gtm_getrlimit(void);
int4 getstorage(void);

#endif /* GETSTORAGE_INCLUDED */
