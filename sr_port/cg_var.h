/****************************************************************
 *								*
 * Copyright (c) 2001-2024 Fidelity National Information	*
 * Services, Inc. and/or its subsidiaries. All rights reserved.	*
 *								*
 *	This source code contains the intellectual property	*
 *	of its copyright holder(s), and is made available	*
 *	under a license.  If you do not know the terms of	*
 *	the license, please stop and do not read further.	*
 *								*
 ****************************************************************/

#ifndef CG_VAR_H_INCLUDED
#define CG_VAR_H_INCLUDED

void cg_var(mtreenode *node, void *var_tabent_arg);
void ind_cg_var(mtreenode *node, void *var_tabent_arg);

#endif
