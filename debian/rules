#!/usr/bin/make -f
# -*- makefile -*-
# debian/rules for fis-gtm

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

include /usr/share/dpkg/default.mk

MARCH := $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
ARCH := $(shell dpkg-architecture -qDEB_HOST_GNU_CPU)
UAPIVER := $(shell echo $(DEB_VERSION_UPSTREAM) | sed -e 's,+.*,,')
UAPIDIR := V$(UAPIVER)_$(ARCH)
BINPKG := $(shell awk '/Package:.*[0-9]/{print $$2;}' debian/control)
GTM_INSTALL_DIR := lib/$(MARCH)/fis-gtm/$(UAPIDIR)
LOCAL_GTM_INSTALL_DIR := $(CURDIR)/debian/$(BINPKG)/usr/$(GTM_INSTALL_DIR)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- -DCMAKE_INSTALL_PREFIX:PATH=/usr -DGTM_INSTALL_DIR=$(GTM_INSTALL_DIR) -DCMAKE_BUILD_TYPE=Release

override_dh_auto_install:
	#echo "umask is: "
	#umask
	dh_auto_install --destdir=debian/$(BINPKG)-stage1 $@
	cd debian/$(BINPKG)-stage1/usr/$(GTM_INSTALL_DIR) && \
		gtm_destdir=$(CURDIR)/debian/$(BINPKG) \
		gtmcompile="-embed_source" \
		./gtminstall --utf8 --user root --group root --verbose \
			--noprompt-for-sys-cfg --distrib . \
			--installdir $(LOCAL_GTM_INSTALL_DIR)
	#@echo "I: Removing redundant license file. One is already available."
	find $(LOCAL_GTM_INSTALL_DIR) -name COPYING -delete
	# remove empty directories
	find $(LOCAL_GTM_INSTALL_DIR) -type d -empty -delete
	# fix permissions of directories
	find $(LOCAL_GTM_INSTALL_DIR) -type d -exec chmod u+x \{\} \;
	# move some binaries from stage1 to $(BINPKG)
	mv debian/$(BINPKG)-stage1/usr/$(GTM_INSTALL_DIR)/gtcm_* debian/$(BINPKG)/usr/$(GTM_INSTALL_DIR)

override_dh_fixperms:
	#echo "umask is: "
	#umask
	dh_fixperms
	#echo "overriding dh_fixperms"
	#echo "I: Fixing up permissions for removed write rights -- we aren't done yet!"
	chmod +w -R $(LOCAL_GTM_INSTALL_DIR)
	chmod +w -R $(LOCAL_GTM_INSTALL_DIR)/utf8
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/utf8
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/plugin
	#echo "I: Fixing up permissions for setuid rights -- we aren't done yet!"
	chmod 4755 $(LOCAL_GTM_INSTALL_DIR)/gtmsecshr
	chmod 0500 $(LOCAL_GTM_INSTALL_DIR)/gtmsecshrdir
	chmod 4500 $(LOCAL_GTM_INSTALL_DIR)/gtmsecshrdir/gtmsecshr
	#echo "I: changing permissions to 644 for files that will be sourced"
	chmod 0644 $(LOCAL_GTM_INSTALL_DIR)/gtmprofile
	chmod 0644 $(LOCAL_GTM_INSTALL_DIR)/gtmprofile_preV54000
	chmod 0644 $(LOCAL_GTM_INSTALL_DIR)/gtcm_slist
	chmod 0644 $(LOCAL_GTM_INSTALL_DIR)/gtmcshrc
	#echo "I: Fixing executable permissions"
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/dse
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_gnp_server
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_pkdisp
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_play
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_run
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_server
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtcm_shmclean
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtm
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtmbase
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtmstart
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/gtmstop
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/lke
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/mumps
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/mupip
	chmod 0755 $(LOCAL_GTM_INSTALL_DIR)/plugin/gtmcrypt/*.sh

	#echo "I: Final permissions after dh_fixperms"
	#ls -l $(LOCAL_GTM_INSTALL_DIR)

# yoh: although it might sound to remove write permissions for the
# deployed materials, as far as I see it, it is useless, since the
# user (root) would always be able to revert them to become writable.

# Because 'the' binary package name will change, let's just generate
# override files
# The override is needed according to upstream for GTM to generate executable 'call-in's
override_dh_lintian:
	echo "$(BINPKG) binary: shared-lib-without-dependency-information [usr/$(GTM_INSTALL_DIR)/libgtmutil.so]" >> debian/$(BINPKG).lintian-overrides
	echo "# The following permissions are really set intentionally in override_dh_fixpermissions" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): elevated-privileges 4500 root/root [usr/$(GTM_INSTALL_DIR)/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): elevated-privileges 4500 root/root [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): executable-is-not-world-readable 4500 [usr/$(GTM_INSTALL_DIR)/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): executable-is-not-world-readable 4500 [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): elevated-privileges 4755 root/root [usr/$(GTM_INSTALL_DIR)/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): elevated-privileges 4755 root/root [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): non-standard-dir-perm 0500 != 0755 [usr/$(GTM_INSTALL_DIR)/gtmsecshrdir/]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): non-standard-setuid-executable-perm 4500 [usr/$(GTM_INSTALL_DIR)/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): non-standard-setuid-executable-perm 4500 [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): package-contains-hardlink pointing to: usr/$(GTM_INSTALL_DIR)/gtmsecshr [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): package-contains-hardlink pointing to: usr/$(GTM_INSTALL_DIR)/gtmsecshrdir/gtmsecshr [usr/$(GTM_INSTALL_DIR)/utf8/gtmsecshrdir/gtmsecshr]" >> debian/$(BINPKG).lintian-overrides
	echo "$(BINPKG): shared-library-lacks-prerequisites [usr/$(GTM_INSTALL_DIR)/utf8/libgtmutil.so]" >> debian/$(BINPKG).lintian-overrides

	dh_lintian

override_dh_auto_clean:
	dh_auto_clean
	rm -rf debian/$(BINPKG)-stage1
	rm -f debian/fis-gtm.links debian/fis-gtm-$(DEB_VERSION_UPSTREAM).lintian-overrides
